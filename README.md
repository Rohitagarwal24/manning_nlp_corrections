The repository contains the corrections done for the manning NLP series "Build a Search Tool with NLP"

1. part1: this contains the updated solution files for "Text Search with spaCy and scikit-learn"
2. part2: this contains the updated solution files for "Implement Semantic Search with ML and BERT"
3. part3: this contains the updated solution files for "Building a Search API with Elasticsearch and BERT"


