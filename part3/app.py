from flask_cors import CORS, cross_origin
from flask import Flask, Response
import json
from elasticsearch import Elasticsearch
from sentence_transformers import SentenceTransformer, util
import sys
import os
app = Flask(__name__)
CORS(app, expose_headers='Location')

port = int(os.getenv("PORT", sys.argv[1]))


@ app.route('/<query>', methods=['GET'])
def search(query, index="pandemics", top_k=10, return_k=5):

    client = Elasticsearch("localhost:9200")

    """
    We use a boolean query to exclude irrelevant sections, but you choose another query type
    if you feel like it returns better results or easier to use
    """

    exclude_sections = [
        "See also",
        "Further reading",
        "Data and graphs",
        "Medical journals",
        "External links"]

    query_body = {"size": return_k,
                  "query": {
                      "bool": {
                          "should": {
                              "match": {"text": query}
                          },
                          "must_not": {
                              "terms": {"section_title.keyword": exclude_sections}
                          },
                      }
                  }
                  }

    # Full text search within an ElasticSearch index (''=all indexes) for the indicated text

    docs = client.search(index=index, body=query_body)

    # Reshape search results to prepare them for sentence embeddings

    search_results = []
    section_titles = []
    article_titles = []

    for result in docs['hits']['hits']:
        search_results.append(result["_source"]["text"])
        section_titles.append(result["_source"]["section_title"])
        article_titles.append(result["_source"]["article_title"])

    embedder = SentenceTransformer('distilbert-base-nli-stsb-mean-tokens')
    corpus_embeddings = embedder.encode(search_results, convert_to_tensor=True)
    query_embedding = embedder.encode(query, convert_to_tensor=True)

    reranked_results = util.semantic_search(
        query_embedding, corpus_embeddings, top_k=top_k)[0]

    reshaped_results = []
    for item in reranked_results:
        idx = item['corpus_id']
        result = {"article_title": article_titles[idx],
                  "section_title": section_titles[idx],
                  "text": search_results[idx],
                  "score": float(item['score']),
                  "count_before_reranking": int(idx)}

        reshaped_results.append(result)

    results_dict = json.dumps({"query": query,
                               "results":  reshaped_results})
    print(results_dict)
    return (Response(results_dict, status=422, mimetype='application/json'), 200)

if __name__ == '__main__':
    #app.run(debug=True)
    app.run(host='0.0.0.0', port=port, debug=True)

# # prevent cached responses
if app.config["DEBUG"]:
    @ app.after_request
    def after_request(response):
        response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate, public, max-age=0"
        response.headers["Expires"] = 0
        response.headers["Pragma"] = "no-cache"
        return response
